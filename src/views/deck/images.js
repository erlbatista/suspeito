
import React from 'react';

const images = {
    free: require('src/views/res/images/free.png'),
    cozinheiro: require('src/views/res/images/cozinheiro.png'),
    empregada: require('src/views/res/images/empregada.png'),
    esposa: require('src/views/res/images/esposa.png'),
    jardineiro: require('src/views/res/images/jardineiro.png'),
    mordomo: require('src/views/res/images/mordomo.png'),
    motorista: require('src/views/res/images/motorista.png'),
    vizinho: require('src/views/res/images/vizinho.png'),
    
    abajur: require('src/views/res/images/abajur.png'),
    chaveDeBoca: require('src/views/res/images/chaveDeBoca.png'),
    faca: require('src/views/res/images/faca.png'),
    martelo: require('src/views/res/images/martelo.png'),
    revolver: require('src/views/res/images/revolver.png'),
    tesoura: require('src/views/res/images/tesoura.png'),
    veneno: require('src/views/res/images/veneno.png'),
    
    banheiro: require('src/views/res/images/banheiro.png'),
    biblioteca: require('src/views/res/images/biblioteca.png'),
    cozinha: require('src/views/res/images/cozinha.png'),
    escadaria: require('src/views/res/images/escadaria.png'),
    hallCentral: require('src/views/res/images/hallCentral.png'),
    quarto: require('src/views/res/images/quarto.png'),
    salaDeJantar: require('src/views/res/images/salaDeJantar.png'),
    suite: require('src/views/res/images/suite.png'),
    varanda: require('src/views/res/images/varanda.png'),
}

export default images;