
import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Image, Text, TouchableOpacity } from 'react-native';

import images from 'src/views/deck/images';

export default class DeckScreen extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            cozinheiroFree: false,
            empregadaFree: false,
            esposaFree: false,
            jardineiroFree: false,
            mordomoFree: false,
            motoristaFree: false,
            vizinhoFree: false,

            abajurFree: false,
            chaveDeBocaFree: false,
            facaFree: false,
            marteloFree: false,
            revolverFree: false,
            tesouraFree: false,
            venenoFree: false,

            banheiroFree: false,
            bibliotecaFree: false,
            cozinhaFree: false,
            escadariaFree: false,
            hallCentralFree: false,
            quartoFree: false,
            salaDeJantarFree: false,
            suiteFree: false,
            varandaFree: false
        }
    }

    render() {
        return (
            <ScrollView style = {deckStyles.container}>
                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({cozinheiroFree: !this.state.cozinheiroFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.cozinheiro} />
                            {this.state.cozinheiroFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Cozinheiro</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({empregadaFree: !this.state.empregadaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.empregada} />
                            {this.state.empregadaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Empregada</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({esposaFree: !this.state.esposaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.esposa} />
                            {this.state.esposaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Esposa</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({jardineiroFree: !this.state.jardineiroFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.jardineiro} />
                            {this.state.jardineiroFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Jardineiro</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({mordomoFree: !this.state.mordomoFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.mordomo} />
                            {this.state.mordomoFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Mordomo</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({motoristaFree: !this.state.motoristaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.motorista} />
                            {this.state.motoristaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Motorista</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({vizinhoFree: !this.state.vizinhoFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.vizinho} />
                            {this.state.vizinhoFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Vizinho</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({abajurFree: !this.state.abajurFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.abajur} />
                            {this.state.abajurFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Abajur</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({chaveDeBocaFree: !this.state.chaveDeBocaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.chaveDeBoca} />
                            {this.state.chaveDeBocaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Chave de Boca</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({facaFree: !this.state.facaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.faca} />
                            {this.state.facaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Faca</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({marteloFree: !this.state.marteloFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.martelo} />
                            {this.state.marteloFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Martelo</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({revolverFree: !this.state.revolverFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.revolver} />
                            {this.state.revolverFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Revolver</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({tesouraFree: !this.state.tesouraFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.tesoura} />
                            {this.state.tesouraFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Tesoura</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({venenoFree: !this.state.venenoFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.veneno} />
                            {this.state.venenoFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Veneno</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({banheiroFree: !this.state.banheiroFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.banheiro} />
                            {this.state.banheiroFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Banheiro</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({bibliotecaFree: !this.state.bibliotecaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.biblioteca} />
                            {this.state.bibliotecaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Biblioteca</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({cozinhaFree: !this.state.cozinhaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.cozinha} />
                            {this.state.cozinhaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Cozinha</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({escadariaFree: !this.state.escadariaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.escadaria} />
                            {this.state.escadariaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Escadaria</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({hallCentralFree: !this.state.hallCentralFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.hallCentral} />
                            {this.state.hallCentralFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Hall Central</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({quartoFree: !this.state.quartoFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.quarto} />
                            {this.state.quartoFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Quarto</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style = {deckStyles.linhaCartas}>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({salaDeJantarFree: !this.state.salaDeJantarFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.salaDeJantar} />
                            {this.state.salaDeJantarFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Sala de Jantar</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({suiteFree: !this.state.suiteFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.suite} />
                            {this.state.suiteFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Suíte</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style = {deckStyles.containerCarta}
                        onPress = {()=>{
                            this.setState({varandaFree: !this.state.varandaFree})
                        }}>
                        <View style = {deckStyles.containerCartaImg}>
                            <Image
                                style = {deckStyles.cartaImg}
                                source = {images.varanda} />
                            {this.state.varandaFree ? 
                                <Image
                                    style = {deckStyles.cartaImg}
                                    source = {images.free} />
                            :
                                null
                            }
                        </View>
                        <View style = {deckStyles.containerCartaSub}>
                            <Text>Varanda</Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        );
    }
}

const deckStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#286636'
    },
    linhaCartas: {
        flexDirection: 'row',
        height: 150,
        justifyContent: 'space-around'
    },
    containerCarta: {
        padding: 5,
        width: 100,
        backgroundColor: '#F5FAFF',
        borderRadius: 5,
        marginVertical: 10,
        elevation: 5
    },
    containerCartaImg: {
        flex: 4
    },
    containerCartaSub: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    cartaImg: {
        position: 'absolute',
        resizeMode: 'stretch',
        height: 100,
        width: 90
    }
});
