
import React from 'react';
import DeckScreen from 'src/views/deck/DeckScreen';

const App = props =>(
    <DeckScreen />
);

export default App;
